package com.agiletestingalliance;


import org.junit.Test;
import static org.junit.Assert.*;



public class MinMaxTest {

	@Test
	public void testCompare() throws Exception {
        	
		int result = new MinMax().compare(1,2);
		assertEquals("firstIf",1, result);
		
	}

        @Test
        public void testCompareSecondIfStatement() throws Exception {

                int result = new MinMax().compare(3,2);
                assertEquals("secondIf",2, result);

        }

        @Test
        public void testBar() throws Exception {

                String result = new MinMax().bar("string");
		assertTrue("secondIf",result.equals("string"));
               
        }


}
