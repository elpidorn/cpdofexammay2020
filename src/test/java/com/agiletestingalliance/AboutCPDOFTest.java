package com.agiletestingalliance;


import org.junit.Test;
import static org.junit.Assert.*;



public class AboutCPDOFTest {

	@Test
	public void testAboutCPDOF() throws Exception {
        	
		String result = new AboutCPDOF().desc();
		assertTrue("AboutCPDOF",result.contains("CP-DOF certification program"));

	}

}
